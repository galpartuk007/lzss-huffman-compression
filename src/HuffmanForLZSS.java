/**
 * LZSS + GUI project Submitted By: 
 * Michael Chen. ID# 315922666 
 * Or Arbiv. ID#209101195 
 * Gal Partuk. ID# 208375691
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Stack;

public class HuffmanForLZSS {

	private HashMap<Byte, Integer> huffmanFrequencyCount;
	private HashMap<String, Byte[]> huffmanDictionary;
	private int numberOfBytes;
	private int numberOfNodes;

	public HuffmanForLZSS() {
		huffmanFrequencyCount = new HashMap<>();
		numberOfBytes = 0;
		numberOfNodes = 0;
	}

	public void compress(String inputPath, String outputPath) throws IOException {
		System.out.println("Huffman compression started");
		File inputFile = new File(inputPath);
		BitInputStream input = new BitInputStream(inputFile);
		BitOutputStream output = new BitOutputStream(new FileOutputStream(outputPath));
		HashMap<Byte, Integer[]> huffmanDictionary = new HashMap<>();
		PriorityQueue<HuffmanNode> huffmanMinHeap = new PriorityQueue<>();
		
		// insert all huffman-nodes into a min-heap ordered by their frequency value
		for (Entry<Byte, Integer> e : huffmanFrequencyCount.entrySet()) {
			numberOfNodes++;
			huffmanMinHeap.add(new HuffmanNode(e.getValue(), e.getKey()));
		}

		while (huffmanMinHeap.size() > 1) {
			HuffmanNode leftNode = huffmanMinHeap.poll();
			HuffmanNode rightNode = huffmanMinHeap.poll();
			HuffmanNode newRoot = new HuffmanNode(leftNode.getFrequency() + rightNode.getFrequency());
			newRoot.setLeftChild(leftNode);
			newRoot.setRightChild(rightNode);
			huffmanMinHeap.offer(newRoot);
		}
		// This is the root of the huffman tree, |left = 0|right = 1|
		HuffmanNode root = huffmanMinHeap.poll();

		// huffmanDictionary should be all set after this function
		// also header data should be written in the file at this point
		// lzss header
		int lzssHeader = input.readBits(32);
		output.writeBits(32, lzssHeader);
		lzssHeader = input.readBits(32);
		output.writeBits(32, lzssHeader);

		// huffman header
		output.writeBits(32, numberOfBytes);
		output.writeBits(32, numberOfNodes);
		CreateHuffmanDictAndHeader(root, 0, 0, huffmanDictionary, output);

		int counter = 0;
		while (counter < numberOfBytes) {
			// try reading first two bytes
			int bit = input.readBits(1);

			if (bit == -1) {
				break;
			}

			if (bit == 1) {
				output.writeBits(1, 1);
				byte leftToupleElement = (byte) input.readBits(8);
				byte rightToupleElement = (byte) input.readBits(8);

				Integer[] data = huffmanDictionary.get(leftToupleElement);
				int dataToWrite = data[0];
				int bitsToWrite = data[1];
				output.writeBits(bitsToWrite, dataToWrite);

				data = huffmanDictionary.get(rightToupleElement);
				dataToWrite = data[0];
				bitsToWrite = data[1];
				output.writeBits(bitsToWrite, dataToWrite);
				counter += 2;
			}

			if (bit == 0) {
				output.writeBits(1, 0);
				byte byteData = (byte) input.readBits(8);
				Integer[] data = huffmanDictionary.get(byteData);
				int dataToWrite = data[0];
				int bitsToWrite = data[1];
				output.writeBits(bitsToWrite, dataToWrite);
				counter++;
			}
		}

		output.flush();
		output.close();
		input.close();
		inputFile.delete();
		System.out.println("Huffman compression ended");
	}

	public int initDecompressionData(BitInputStream input, BitOutputStream output) throws IOException {
		System.out.println("Buiding Huffman tree");

		Stack<HuffmanNode> stack = new Stack<>();
		huffmanDictionary = new HashMap<>();

		numberOfBytes = input.readBits(32);
		int numberOfLeaf = input.readBits(32);
		int numberOfInternalNodes = numberOfLeaf - 1;

		// start building huffman tree from header data
		int bit = input.readBits(1);
		while (numberOfLeaf != 0 || numberOfInternalNodes != 0) {
			if (bit == 1) {
				stack.push(new HuffmanNode((byte) input.readBits(8)));
				bit = input.readBits(1);
				numberOfLeaf--;
			} else if (bit == 0) {
				HuffmanNode rightNode = stack.pop();
				HuffmanNode leftNode = stack.pop();
				HuffmanNode father = new HuffmanNode();
				father.setLeftChild(leftNode);
				father.setRightChild(rightNode);
				stack.push(father);
				bit = input.readBits(1);
				numberOfInternalNodes--;
			}
		}
		CreateHuffmanDictionary(stack.pop(), "", (byte) 0, huffmanDictionary);
		System.out.println("Huffman tree is built");
		return bit;
	}

	public void insertByteIntoFrequencyCounter(byte data) {
		huffmanFrequencyCount.put(data, huffmanFrequencyCount.getOrDefault(data, 0) + 1);
		numberOfBytes++;
	}

	// this function take a huffman tree and initializes a dictionary based on it
	// this function also takes a list and inserts the header data into it
	private void CreateHuffmanDictAndHeader(HuffmanNode root, int code, int count, HashMap<Byte, Integer[]> dict,
			BitOutputStream output) {
		if (root.getLeftChild() == null && root.getRightChild() == null) {
			byte symbol = root.getByteData();
			Integer[] data = new Integer[] { code, count };
			dict.put(symbol, data);
			output.writeBits(1, 1);
			output.writeBits(8, symbol);
			return;
		}
		CreateHuffmanDictAndHeader(root.getLeftChild(), code << 1, count + 1, dict, output);
		CreateHuffmanDictAndHeader(root.getRightChild(), (code << 1) + 1, count + 1, dict, output);
		output.writeBits(1, 0);
	}

	private void CreateHuffmanDictionary(HuffmanNode root, String code, byte count, HashMap<String, Byte[]> dict) {
		if (root.getLeftChild() == null && root.getRightChild() == null) {
			byte symbol = root.getByteData();
			Byte[] data = new Byte[] { symbol, count };
			dict.put(code, data);
			return;
		}
		CreateHuffmanDictionary(root.getLeftChild(), code + "0", (byte) (count + 1), dict);
		CreateHuffmanDictionary(root.getRightChild(), code + "1", (byte) (count + 1), dict);
	}
	
	public Byte[] getValue(BitInputStream input, BitOutputStream output) throws IOException {		
		String bit = Integer.toBinaryString(input.readBits(1));
		int length = 1;
		while (numberOfBytes > 0) {
			if (huffmanDictionary.containsKey(bit)) {
				Byte[] value = huffmanDictionary.get(bit);
				int len = value[1];
				if (len == length) {
					numberOfBytes--;
					return value;
				}
			}
			bit = bit + Integer.toBinaryString(input.readBits(1));
			length++;
		}
		return null;
	}
}
