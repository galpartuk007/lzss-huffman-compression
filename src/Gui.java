/**
 * LZSS + GUI project Submitted By: 
 * Michael Chen. ID# 315922666 
 * Or Arbiv. ID#209101195 
 * Gal Partuk. ID# 208375691
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.*;
import javax.swing.*;

   class Gui{
	   JFrame frame =  new JFrame("LZSS COMPRESSOR");
	   LZSSCompressor lzss = new LZSSCompressor();
	   JPanel selectionPanel = new JPanel();
	   public Gui(){
		  
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.setSize(600,500);
	        JLabel selectionLabel = new JLabel("select process");
	        JButton compressSelect = new JButton("compress");
	        JButton decompressSelect = new JButton("decompress");
	        selectionPanel.add(selectionLabel);
	        selectionPanel.add(compressSelect);
	        selectionPanel.add(decompressSelect);
	        frame.getContentPane().add(BorderLayout.CENTER,selectionPanel);
	        frame.setVisible(true);
	        compressSelect.addActionListener(new ActionListener() {
	          	@Override
	          	public void actionPerformed(ActionEvent e) {
	          		frame.remove(selectionPanel);;
	          		compression();
	              }
	          });
	        decompressSelect.addActionListener(new ActionListener() {
	          	@Override
	          	public void actionPerformed(ActionEvent e) {
	          		frame.remove(selectionPanel);;
	          		decompression();
	              }
	          });
	   }

      private void compression() {
    	  frame.setTitle("compression");
    	  JButton selectInputButton = new JButton("select input file");
          JButton compressButton = new JButton("Compress");
          JPanel inputPanel = new JPanel();
          JLabel inputLabel = new JLabel("File Path");
          JLabel fileName = new JLabel();
          JTextField inputPathField = new JTextField(20);
          try {
  			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
  		} catch (ClassNotFoundException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (InstantiationException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (IllegalAccessException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (UnsupportedLookAndFeelException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
          inputPanel.add(inputLabel);
          inputPanel.add(inputPathField);
          inputPanel.add(selectInputButton);
          selectInputButton.addActionListener(new ActionListener() {
          	@Override
          	public void actionPerformed(ActionEvent e) {
          		JFileChooser input = new JFileChooser();
          		int returnVal = input.showOpenDialog(selectInputButton);
          		if(returnVal == JFileChooser.APPROVE_OPTION) {
          		File inputFile = input.getSelectedFile();
          		inputPathField.setText(inputFile.getAbsolutePath());
          		inputPathField.setVisible(true);
          		fileName.setText(inputFile.getName());
          		fileName.setVisible(true);
          		inputPanel.add(fileName,3);
          		frame.setVisible(true);
          		}
              }
          });
          JButton selectOutputButton = new JButton("Select Output Directory");
          JPanel outputPanel = new JPanel();
          JLabel outputLabel = new JLabel("Output Path");
          JTextField outputPathField = new JTextField(20);
          outputPanel.add(outputLabel);
          outputPanel.add(outputPathField);
          outputPanel.add(selectOutputButton);
          outputPanel.add(compressButton);
          selectOutputButton.addActionListener(new ActionListener() {
          	public void actionPerformed(ActionEvent e) {
          		JFileChooser f = new JFileChooser();
          		f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
                  int returnVal = f.showSaveDialog(null);
                  if(returnVal == JFileChooser.APPROVE_OPTION) {
          		outputPathField.setText(f.getSelectedFile().getAbsolutePath());
          		outputPathField.setVisible(true);
          		inputPanel.add(fileName,3);
          		frame.setVisible(true);
                  }
          	}
          });
          compressButton.addActionListener(new ActionListener() {
          	public void actionPerformed(ActionEvent e) {
          		if(inputPathField.getText().isEmpty()) {
          			JOptionPane.showMessageDialog(frame, "no file was selected");
          		}
          		else if(outputPathField.getText().isEmpty()){
          			JOptionPane.showMessageDialog(frame, "no directory was selected as an output location");
          		}
          		else {
          			String str = fileName.getText();
//          			int index = 0;
//          			for (int i = str.length() - 1; i > 0; i--) {
//          				if (str.charAt(i) == '.') {
//          					index = i;
//          					break;
//          				}
//          			}
          			int index = str.lastIndexOf('.');
          			String output = str.substring(0,index) + " Compressed" + str.substring(index);
          			
          			try {
  						lzss.Compress(inputPathField.getText(),outputPathField.getText() + "\\temp.txt",outputPathField.getText() + "\\" + output);
  					} catch (IOException e1) {
  						// TODO Auto-generated catch block
  						e1.printStackTrace();
  					}
          			JOptionPane.showMessageDialog(frame, "Compression is completed");
          			
          		}
          	}
          });
          JButton returnToTop = new JButton("back to top");
          returnToTop.addActionListener(new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent e) {
            		frame.remove(inputPanel);
            		frame.remove(outputPanel);
            		frame.remove(returnToTop);
            		frame.setVisible(false);
        	        frame.getContentPane().add(BorderLayout.CENTER,selectionPanel);
        	        frame.setVisible(true);
                }
            });
         frame.getContentPane().add(BorderLayout.NORTH, inputPanel);
         frame.getContentPane().add(BorderLayout.CENTER, outputPanel);
         frame.getContentPane().add(BorderLayout.SOUTH, returnToTop);
         frame.setVisible(true);
       }
		
	
      private void decompression() {
    	  frame.setTitle("decompression");
    	  JButton selectInputButton = new JButton("select input file");
          JButton compressButton = new JButton("Decompress");
          JPanel inputPanel = new JPanel();
          JLabel inputLabel = new JLabel("File Path");
          JLabel fileName = new JLabel();
          JTextField inputPathField = new JTextField(20);
          try {
  			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
  		} catch (ClassNotFoundException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (InstantiationException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (IllegalAccessException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (UnsupportedLookAndFeelException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
          inputPanel.add(inputLabel);
          inputPanel.add(inputPathField);
          inputPanel.add(selectInputButton);
          selectInputButton.addActionListener(new ActionListener() {
          	@Override
          	public void actionPerformed(ActionEvent e) {
          		JFileChooser input = new JFileChooser();
          		int returnVal = input.showOpenDialog(selectInputButton);
          		if(returnVal == JFileChooser.APPROVE_OPTION) {
          		File inputFile = input.getSelectedFile();
          		inputPathField.setText(inputFile.getAbsolutePath());
          		inputPathField.setVisible(true);
          		fileName.setText(inputFile.getName());
          		fileName.setVisible(true);
          		inputPanel.add(fileName,3);
          		frame.setVisible(true);
          		}
              }
          });
          JButton selectOutputButton = new JButton("Select Output Directory");
          JPanel outputPanel = new JPanel();
          JLabel outputLabel = new JLabel("Output Path");
          JTextField outputPathField = new JTextField(20);
          outputPanel.add(outputLabel);
          outputPanel.add(outputPathField);
          outputPanel.add(selectOutputButton);
          outputPanel.add(compressButton);
          selectOutputButton.addActionListener(new ActionListener() {
          	public void actionPerformed(ActionEvent e) {
          		JFileChooser f = new JFileChooser();
          		f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
                  int returnVal = f.showSaveDialog(null);
                  if(returnVal == JFileChooser.APPROVE_OPTION) {
          		outputPathField.setText(f.getSelectedFile().getAbsolutePath());
          		outputPathField.setVisible(true);
          		inputPanel.add(fileName,3);
          		frame.setVisible(true);
                  }
          	}
          });
          compressButton.addActionListener(new ActionListener() {
          	public void actionPerformed(ActionEvent e) {
          		if(inputPathField.getText().isEmpty()) {
          			JOptionPane.showMessageDialog(frame, "no file was selected");
          		}
          		else if(outputPathField.getText().isEmpty()){
          			JOptionPane.showMessageDialog(frame, "no directory was selected as an output location");
          		}
          		else if(fileName.getText().lastIndexOf("Compressed") == -1) {
          			JOptionPane.showMessageDialog(frame, "not a compressed file");
          		}
          		else {
          			String str = fileName.getText();
          			int index = str.lastIndexOf('.');
          			String output = str.substring(0,str.lastIndexOf("Compressed")) + "Decompressed" + str.substring(index);
          			try {
  						lzss.Decompress(inputPathField.getText(), outputPathField.getText() + "\\" + output);
  					} catch (IOException e1) {
  						// TODO Auto-generated catch block
  						e1.printStackTrace();
  					}
          			JOptionPane.showMessageDialog(frame, "Decompression is completed");
          		}
          	}
          });
          JButton returnToTop = new JButton("back to top");
          returnToTop.addActionListener(new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent e) {
            		frame.remove(inputPanel);
            		frame.remove(outputPanel);
            		frame.remove(returnToTop);
            		frame.setVisible(false);
        	        frame.getContentPane().add(BorderLayout.CENTER,selectionPanel);
        	        frame.setVisible(true);
                }
            });
         frame.getContentPane().add(BorderLayout.NORTH, inputPanel);
         frame.getContentPane().add(BorderLayout.CENTER, outputPanel);
         frame.getContentPane().add(BorderLayout.SOUTH, returnToTop);
         frame.setVisible(true);
      }

	public static void main(String args[]){
        new Gui();
}
	}