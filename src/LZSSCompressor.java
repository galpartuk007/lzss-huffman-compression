/**
 * LZSS + GUI project Submitted By: 
 * Michael Chen. ID# 315922666 
 * Or Arbiv. ID#209101195 
 * Gal Partuk. ID# 208375691
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class LZSSCompressor {
	private BitInputStream bitInput;
	private BitOutputStream bitOutput;
	private FileInputStream byteInput;
	private int blockNumber;
	private byte[] dataBlock;
	private int absIndex;
	private int lookAheadBufferIndex;
	private int maxMatch;
	private boolean hasToEnd;
	private long numberOfBytes;
	private long numberOfBytesRead;
	private HuffmanForLZSS huffman;

	private final boolean DEBUG = false;
	private final int DATA_BLOCK_SIZE = (int) Math.pow(2, 13); // 13 BYTE block size (8192 symbols)
	private final int THRESHOLD = 3;
	private final int SEARCH_BUFFER_LENGTH = 4096; // 12 bit hence the offset can be from 1-12
	private final int LOOK_AHEAD_BUFFER_LENGTH = 16; // 4 bit hence we can compress only strings of length 16
	// (1-12,3-16) <--- compression tuple(size 2 bytes == 16bit), we should
	// compress with LZSS only if the
	// length of the string is 3 and above

	public LZSSCompressor() {
		bitInput = null;
		bitOutput = null;
		byteInput = null;
		huffman = new HuffmanForLZSS();
	}

	public void Compress(String inputPath, String temporaryFile,String outputPath) throws IOException { // compress need to get temp location from gui
		File inputFile = new File(inputPath);
		numberOfBytes = inputFile.length();
		numberOfBytesRead = 0;
		System.out.println("Starting compression for file:" + inputFile.getName());

		byteInput = new FileInputStream(inputPath);// this is where the file is read from
		bitOutput = new BitOutputStream(new FileOutputStream(temporaryFile));
		dataBlock = new byte[DATA_BLOCK_SIZE];

		// insert header data
		int firstByte = (int) (numberOfBytes >>> 32);
		int secondByte = (int) (numberOfBytes);
		bitOutput.writeBits(32, firstByte);
		bitOutput.writeBits(32, secondByte);

		HashMap<String, LinkedList<Integer>> hashMap = new HashMap<>(); // (key: <3 length string> => value: <integer>)

		absIndex = 0;
		lookAheadBufferIndex = 0;
		hasToEnd = false;
		maxMatch = 0;
		blockNumber = 0;
		char[] symbol = new char[THRESHOLD];

		readDataBlock(true, hashMap); // read 13 Byte data into out data buffer(dataBlock[])

		// start compression
		while (!hasToEnd) {
			for (int i = 0; i < THRESHOLD; i++) {
				if (absIndex >= DATA_BLOCK_SIZE - 2) {
					readDataBlock(false, hashMap);
					i = 0;
				}
				if (i + numberOfBytesRead >= numberOfBytes) {
					hasToEnd = true;
					for (int j = 0; j < i; j++) {
						bitOutput.writeBits(1, 0);
						bitOutput.writeBits(8, symbol[j]);
						huffman.insertByteIntoFrequencyCounter((byte) symbol[j]);
					}
					break;
				}
				symbol[i] = (char) dataBlock[absIndex + i];
			}

			if (hasToEnd) {
				break;
			}

			LinkedList<Integer> ls = getFromHashMap(String.valueOf(symbol), hashMap);
			if (!ls.isEmpty() && absIndex + 1 - ls.peek() < SEARCH_BUFFER_LENGTH) {

				int currentMatch = 0;
				int absToRemember = 0;
				int temp = 0;

				for (Integer data : ls) {

					if (absIndex + 1 - data >= SEARCH_BUFFER_LENGTH) {
						for (int i = ls.size() - 1; i >= temp; i--) {
							ls.remove(i);
						}
						break;
					}

					for (int i = 0; (currentMatch < LOOK_AHEAD_BUFFER_LENGTH - 1)
							&& (absIndex + i < DATA_BLOCK_SIZE); i++) {
						if (dataBlock[data + i - 1] == dataBlock[absIndex + i]) {
							currentMatch++;
						} else {
							break;
						}
					}
					if (maxMatch < currentMatch) {
						maxMatch = currentMatch;
						absToRemember = data;
					}
					currentMatch = 0;
					temp++;
				}

				bitOutput.writeBits(1, 1);
				bitOutput.writeBits(12, absIndex + 1 - absToRemember);
				bitOutput.writeBits(4, maxMatch);
				System.out.println("offset "+(absIndex + 1 - absToRemember));
				System.out.println("maxMatch "+maxMatch);
				int twoTuple = (((absIndex + 1 - absToRemember) << 4) | maxMatch);
				huffman.insertByteIntoFrequencyCounter((byte) ((twoTuple & 0x0000ffff) >>> 8));
				huffman.insertByteIntoFrequencyCounter((byte) (twoTuple & 0x000000ff));
				lookAheadBufferIndex = lookAheadBufferIndex + maxMatch;

			} else {
				lookAheadBufferIndex++;
				bitOutput.writeBits(1, 0);
				bitOutput.writeBits(8, symbol[0]);
				System.out.println("else "+symbol[0]);
				huffman.insertByteIntoFrequencyCounter((byte) symbol[0]);
				maxMatch = 1;
			}
			ls.addFirst(absIndex + 1);
			absIndex += maxMatch;
			numberOfBytesRead += maxMatch;
			maxMatch = 0;
		}
		bitOutput.flush();
		byteInput.close();
		bitOutput.close();
		huffman.compress(temporaryFile, outputPath);
		// delete temp here
		System.out.println("Finished Compression");
		System.out.println("File at:\n" + outputPath);
	}

	public void Decompress(String inputPath, String outputPath) throws IOException {
		File inputFile = new File(inputPath);
		System.out.println("Starting decompression for file:" + inputFile.getName());
		bitInput = new BitInputStream(new FileInputStream(inputPath));
		bitOutput = new BitOutputStream(new FileOutputStream(outputPath));
		byte readBuffer[] = new byte[DATA_BLOCK_SIZE];

		numberOfBytes = bitInput.readBits(32);
		numberOfBytes <<= 32;
		numberOfBytes |= bitInput.readBits(32);

		numberOfBytesRead = 0;
		absIndex = 0;
		blockNumber = 1;
		System.out.println("Block number - " + blockNumber);

		int bit = huffman.initDecompressionData(bitInput, bitOutput);
		while (numberOfBytes != numberOfBytesRead) {
			if (absIndex < DATA_BLOCK_SIZE) {
				if (bit == 1) {
					int firstValue = huffman.getValue(bitInput, bitOutput)[0];
					int secondValue = huffman.getValue(bitInput, bitOutput)[0];

					int twoTuple = ((firstValue & 0x000000ff) << 8) | (secondValue & 0x000000ff);
					int offset = twoTuple >>> 4;
					int length = twoTuple & 0x0000000f;

					for (int i = absIndex; i < absIndex + length; i++) {
						readBuffer[i] = readBuffer[i - offset];
					}

					bitOutput.write(Arrays.copyOfRange(readBuffer, absIndex, absIndex + length));
					absIndex += length;
					numberOfBytesRead += length;

					bit = bitInput.readBits(1);
				} else if (bit == 0) {
					Byte[] byteDataArray = huffman.getValue(bitInput, bitOutput);
					if(byteDataArray == null) {
						break;
					}
					byte byteData = byteDataArray[0];
					numberOfBytesRead++;
					readBuffer[absIndex++] = byteData;
					bitOutput.writeBits(8, byteData);
					bit = bitInput.readBits(1);
				}
			} else {
				absIndex = 0;
				blockNumber++;
				System.out.println("Block number - " + blockNumber);
			}
		}
		bitOutput.flush();
		bitOutput.close();
		bitInput.close();
		System.out.println("Decompression Finished");
		System.out.println("File at:\n" + outputPath);
	}

	private void readDataBlock(boolean firstBlock, HashMap<String, LinkedList<Integer>> hashMap) throws IOException {
		blockNumber++;
		System.out.println("Block number - " + blockNumber);
		if (!firstBlock) {
			hashMap.clear();
			for (int i = absIndex; i < DATA_BLOCK_SIZE; i++, absIndex++) {
				bitOutput.writeBits(1, 0);
				bitOutput.writeBits(8, dataBlock[i]);
				huffman.insertByteIntoFrequencyCounter((byte) dataBlock[i]);
			}
			absIndex = 0;
			lookAheadBufferIndex = 16;
		}
		byte byteData;
		int x = 0;
		do {
			byteData = (byte) byteInput.read();
			dataBlock[x++] = byteData;
		} while (numberOfBytes != x && x < DATA_BLOCK_SIZE);
	}

	private LinkedList<Integer> getFromHashMap(String symbol, HashMap<String, LinkedList<Integer>> hashMap) {
		if (hashMap.containsKey(symbol)) {
			return hashMap.get(symbol);
		} else {
			LinkedList<Integer> ls = new LinkedList<Integer>();
			hashMap.put(symbol, ls);
			return ls;
		}
	}
}
