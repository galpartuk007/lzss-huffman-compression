/**
 * LZSS + GUI project Submitted By: 
 * Michael Chen. ID# 315922666 
 * Or Arbiv. ID#209101195 
 * Gal Partuk. ID# 208375691
 */
public class HuffmanNode implements Comparable<HuffmanNode> {

	private HuffmanNode leftChild;
	private HuffmanNode rightChild;
	private int frequency;
	private byte byteData;
	private boolean visited;

	public HuffmanNode(int frequency, byte byteData) {
		this.leftChild = null;
		this.rightChild = null;
		this.frequency = frequency;
		this.byteData = byteData;
		this.visited = false;
	}

	public HuffmanNode(byte byteData) {
		this(0, byteData);
	}

	public HuffmanNode(int frequency) {
		this(frequency, (byte) 0);
	}

	public HuffmanNode() {
		this(0, (byte) 0);
	}

	public HuffmanNode getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(HuffmanNode leftChild) {
		this.leftChild = leftChild;
	}

	public HuffmanNode getRightChild() {
		return rightChild;
	}

	public void setRightChild(HuffmanNode rightChild) {
		this.rightChild = rightChild;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public byte getByteData() {
		return byteData;
	}

	public void setByteData(byte byteData) {
		this.byteData = byteData;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	@Override
	public int compareTo(HuffmanNode hNode) {
		if (this.frequency > hNode.frequency)
			return 1;
		else if (this.frequency < hNode.frequency)
			return -1;
		return 0;
	}
}